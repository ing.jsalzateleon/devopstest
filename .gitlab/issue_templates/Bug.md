Summary 

{Da el resumen de issue}

Steps to reproduce

{Indica los pasos para reproducir el bug}

What's the current Behavior?

What's the expected behavior?